import React, { useEffect } from "react";
import { useDispatch } from "react-redux";

import { refreshToken } from "./redux/actions/userAction";
import "./App.css";
import Router from "./Router";

function App() {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(refreshToken());
    }, [dispatch]);

    return <Router />;
}

export default App;
