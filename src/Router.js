import React from "react";
import { Route, Routes } from "react-router-dom";
import TimerDisplay from "./privatepages/timerDisplay";
import Home from "./publicpages/Home";
import Login from "./publicpages/Login";
import Register from "./publicpages/Register";
import NotFound from "./publicpages/NotFound";

const Router = () => {
    return (
        <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/timer" element={<TimerDisplay />} />
            <Route path="*" element={<NotFound />} />
        </Routes>
    );
};

export default Router;
