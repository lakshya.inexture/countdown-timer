import React from "react";
import { Link, useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import Navbar from "./Navbar";
import timer from "../assets/images/timer_SS.png";
import eCommerce from "../assets/images/ecommerce-app_SS.png";

const Home = () => {
    const navigate = useNavigate();
    const { authReducer } = useSelector((state) => state);

    const redirectTimer = () => {
        Object.keys(authReducer).length > 0 && navigate("/timer");
    };

    return (
        <div className="home">
            <Navbar />
            <h1 className="header">My Projects</h1>
            <div className="head-links-wrapper">
                <div onClick={redirectTimer} className="head-links">
                    <Link to="/timer">Timer</Link>
                    <img src={timer} alt="timer-display-img" />
                </div>
                <a
                    target="_blank"
                    rel="noreferrer"
                    href="https://kharido-dot-com.netlify.app/">
                    <div className="head-links">
                        <span>E-Commerce App</span>
                        <img src={eCommerce} alt="e-commerce-app-display-img" />
                    </div>
                </a>
            </div>
        </div>
    );
};

export default Home;
