import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { logout } from "../redux/actions/userAction";

const Navbar = () => {
    const dispatch = useDispatch();
    const { authReducer } = useSelector((state) => state);
    return (
        <nav className="navbar">
            <ul>
                <li>
                    <Link to="/">Home</Link>
                </li>
                {authReducer.token ? (
                    <li>
                        <Link to="/" onClick={() => dispatch(logout())}>
                            Logout
                        </Link>
                    </li>
                ) : (
                    <>
                        <li>
                            <Link to="/register">Register</Link>
                        </li>
                        <li>
                            <Link to="/login">Login</Link>
                        </li>
                    </>
                )}
                <li>
                    <Link to="/timer">Timer</Link>
                </li>
            </ul>
        </nav>
    );
};

export default Navbar;
