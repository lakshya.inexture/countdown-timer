import React from "react";
import Navbar from "./Navbar";
import notFound from "../assets/images/notfound.gif";

const NotFound = () => {
    return (
        <div className="not-found">
            <Navbar />
            <div className="not-found-wrapper">
                <img src={notFound} alt="" />
            </div>
        </div>
    );
};

export default NotFound;
